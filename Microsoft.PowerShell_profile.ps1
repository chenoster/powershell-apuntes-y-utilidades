##################################################################################
# EJEMPLO PERFIL DE USUARIO                                                      #
#                                                                                #
# Este fichero tiene que estar en la siguiente ruta:                             #
# C:\Users\xxxxxx\Documents\Windows PowerShell\Microsoft.PowerShell_profile.ps1  #
##################################################################################

Write-Host "****************************************** SuperVitaminado por Chenoster ******************************************" -ForegroundColor White -BackgroundColor DarkGreen -NoNewline

$RutaLog = 'C:\Users\xxxxxxxxx\Log.txt'

function log {
 # Param ([String]$Texto) 
 # En lugar de pasar el texto como parámetro (lo cual obliga a escribir comillas), lo hacemos así:
  $Texto=read-host "Por favor, introduce el texto que deseas incluir en el log: (Ctrl + c para cancelar) `n" 
  $Fecha = get-date -format g 
  $LineaLog = $Fecha + '   ' + $Texto
  $LineaLog >> $RutaLog
}

function vlog { 
 get-content $RutaLog
}

function tlog { 
 get-content $RutaLog | Out-GridView
}

function blog { 
 Param ([String]$Texto) 
 get-content $RutaLog | sls -simple $Texto
}

function cmz { 
   Start-Process "C:\Program Files\Microsoft Office\root\Office16\OUTLOOK.EXE"
   Start-Process "C:\Program Files (x86)\Mozilla Thunderbird\thunderbird.exe"
   Start-Process "C:\Program Files (x86)\Mozilla Firefox\firefox.exe"
   Start-Process "C:\Program Files (x86)\BraveSoftware\Brave-Browser\Application\brave.exe"
   Start-Process "C:\Users\xxxxxx\Documents\dbeaver\dbeaver.exe"
   Start-Process "C:\Users\xxxxxxx\Documents\sqldeveloper\sqldeveloper\bin\sqldeveloper64W.exe"

   $shell = New-Object -ComObject "Shell.Application"
   $shell.minimizeall()
}

function busca { 
 Param ([String]$Extension, [String]$Texto)
 
 cd ~
 dir $Extension -rec | sls -simple $Texto | Out-GridView

}

function comprueba-conexiones {

    $servidores=@( ('web','web.com'),
                   ('Correo','correo.com')
                  )
    
    FOREACH ($servidor in $servidores){
    $resultado=Test-Connection $servidor[1] -count 1 -quiet
    
    if($resultado -eq 'True'){
        write-host $servidor '----->' $resultado
     }else{
        write-host $servidor '----->' $resultado -ForegroundColor Red  
     }
    }
    
}

function lista_tamanio { # Lista carpetas que están dentro de la ruta actual indicando su tamaño en MB

Get-ChildItem | Foreach {
$Files = Get-ChildItem $_.FullName -Recurse -File
$Size = '{0:N2}' -f (( $Files | Measure-Object -Property Length -Sum).Sum /1MB)
[PSCustomObject]@{Profile = $_.FullName ; TotalObjects = "$($Files.Count)" ; SizeMB = $Size}
}| Out-GridView 

}
