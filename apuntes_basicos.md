## Apuntes básicos

Los comandos normalmente están formados por un verbo (get, set, write, ...) y un nombre (host, content, profile, ...) unidos por un guión -.  

Ayuda de powershell:   
help: proporciona ayuda en texto formateado  
get-help: ayuda en la salida estándar  

Atajos de teclado:  
TAB para autocompletar  
ctrl <- ->  para ir saltando de palabra en palabra  
F7 F8 y F9 historial de comandos  
Ctrl + c para finalizar proceso  
INICIO y FIN principio o fin de la linea  

Editor (IDE) --> ISE  

Políticas de Ejecución:  
La política por defecto es *restricted*, por lo que no se permite ejecutar scripts, aunque hay trucos para saltárselo  
Se puede modificar con Set-Executionpolicy <politica> (es necesario tener permisos de administración)  
Se puede cambiar a unrestricted o a remotesigned   

Saltarse las politicas de ejecucion:  
1) copiar y pegar el texto del script en la consola   
2) get-content script.ps1 | powershell -noprofile -  
Pongo estas dos como ejemplo pero hay muchas más  

### Perfiles

Hay 4 tipos de perfiles:  

1) usuario todos los escenarios powershell (Usuario\Mis documentos\Windows PowerShell\profile.ps1). Orden de carga 3  
2) usuario powershell instalado por defecto (Usuario\Mis documentos\Windows PowerShell\Microsoft.PowerShell_profile.ps1). Orden de carga 4  
3) Máquina todos los escenarios (system32\WindowsPowerShell\v1.0\profile.ps1). Orden de carga 1  
4) Máquina instalado por defecto (system32\WindowsPowerShell\v1.0\Microsoft.PowerShell_profile.ps1). Orden de carga 2  

Orden de carga, prevalece lo último cargado  

Se puede crear el fichero si no existe y poner ahí alias y funciones personalizadas. Ej.:

~~~
Write-Host "****************************************** SuperVitaminado por Chenoster ******************************************" -ForegroundColor White -BackgroundColor DarkGreen -NoNewline

$RutaLog = 'C:\Users\xxxxxxxxx\Log.txt'

function log {
 # Param ([String]$Texto) 
 # En lugar de pasar el texto como parámetro (lo cual obliga a escribir comillas), lo hacemos así:
  $Texto=read-host "Por favor, introduce el texto que deseas incluir en el log: (Ctrl + c para cancelar) `n" 
  $Fecha = get-date -format g 
  $LineaLog = $Fecha + '   ' + $Texto
  $LineaLog >> $RutaLog
}

function vlog { 
 get-content $RutaLog
}

function tlog { 
 get-content $RutaLog | Out-GridView
}

function blog { 
 Param ([String]$Texto) 
 get-content $RutaLog | sls -simple $Texto
}

function cmz { 
   Start-Process "C:\Program Files\Microsoft Office\root\Office16\OUTLOOK.EXE"
   Start-Process "C:\Program Files (x86)\Mozilla Thunderbird\thunderbird.exe"
   Start-Process "C:\Program Files (x86)\Mozilla Firefox\firefox.exe"
   Start-Process "C:\Program Files (x86)\BraveSoftware\Brave-Browser\Application\brave.exe"
   Start-Process "C:\Users\xxxxxx\Documents\dbeaver\dbeaver.exe"
   Start-Process "C:\Users\xxxxxxx\Documents\sqldeveloper\sqldeveloper\bin\sqldeveloper64W.exe"

   $shell = New-Object -ComObject "Shell.Application"
   $shell.minimizeall()
}

function busca { 
 Param ([String]$Extension, [String]$Texto)
 
 cd ~
 dir $Extension -rec | sls -simple $Texto | Out-GridView

}

function comprueba-conexiones {

    $servidores=@( ('web','web.com'),
                   ('Correo','correo.com'),
                  )
    
    FOREACH ($servidor in $servidores){
    $resultado=Test-Connection $servidor[1] -count 1 -quiet
    
    if($resultado -eq 'True'){
        write-host $servidor '----->' $resultado
     }else{
        write-host $servidor '----->' $resultado -ForegroundColor Red  
     }
    }
    
}

function lista_tamanio { # Lista carpetas que están dentro de la ruta actual indicando su tamaño en MB

Get-ChildItem | Foreach {
$Files = Get-ChildItem $_.FullName -Recurse -File
$Size = '{0:N2}' -f (( $Files | Measure-Object -Property Length -Sum).Sum /1MB)
[PSCustomObject]@{Profile = $_.FullName ; TotalObjects = "$($Files.Count)" ; SizeMB = $Size}
}| Out-GridView 

}
~~~